#!/usr/bin/env python3
"""
ioref: Reference & information manager.

Author: Vlad Topan (vtopan/gmail)
"""

import os
IOR_PATH = os.path.dirname(__file__)
import sys
sys.path += [f'{IOR_PATH}/io-scripts', f'{IOR_PATH}/iolib', f'{IOR_PATH}/libioqt', f'{IOR_PATH}/izd-tools']

from ioref.ui import IorMainWindow


if __name__ == '__main__':
    try:
        sys.exit(IorMainWindow.run(sys.argv))
    except Exception as e:
        sys.stderr.write(f'[!] Dead: {e}\n')
        raise
