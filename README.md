# ioref

Cross-platform reference & information management software. Indexes documents (Markdown, PDF, etc.)
for full text searching. It's written in Python and uses
[Whoosh](https://whoosh.readthedocs.io/en/latest/intro.html) for indexing,
[PySide2](https://pypi.org/project/PySide2/) (Qt5 wrapper) for the UI,
[PyPDF2](https://pypi.org/project/PyPDF2/) or pdftotext (if present in `%PATH%`/`$PATH`).

Vlad Topan, 2022

## Installation

- have [Python 3](https://www.python.org/downloads/) installed (3.6+)
- have `git` installed
- run `git clone --recurse-submodules --depth=1 https://gitlab.com/vtopan/ioref.git` to clone repo
- install dependencies: `cd ioref; python3 -m pip install -r requirements.txt`
- optional, but recommended: ensure [pdftotext](http://www.xpdfreader.com/download.html) from
  *XpdfReader* is in the system `PATH` (it will be used if found, it's more accurate and faster
  than *PyPDF2*)

## Usage

- run `ioref.pyw`
- add documents by copying a URL or a (full) file / folder path to clipboard, then pressing
  `Add document from clipboard (URL or path)` in the `Document Manager` tab
  - all documents are saved in the document storage (default: `data/doc-storage`)
- search for documents in the `Find Documents` tab (use `*` as a wildcard, e.g. `john*`)
- to manually download a document (blog, etc.) from a URL as IZD use
    `$proj/io-scripts/getdoc.py <url> -o izd`
- to view an `.izd` document with a standalone tool, associate the extension with
  `$proj/vizd/vizd.pyw` - run these commands from the project (root) folder:

  ```bat
  reg add HKCU\SOFTWARE\Classes\IZDDocument\DefaultIcon /ve /d "%cd%\ioref\book64.ico" /f
  reg add HKCU\SOFTWARE\Classes\IZDDocument\shell\open\command /ve /d "pythonw.exe \"%cd%\ioref.pyw\" %1" /f
  reg add HKCU\SOFTWARE\Classes\.izd /ve /d "IZDDocument" /f
  ```

  - replace `HKCU\SOFTWARE\Classes` with `HKCR` and run from an Administrator console to make
    the association for all users
- to send documents directly from the browser to IoRef, run the `browser-listener.py` script and
  then run the following JS in the browser's console:

  ```js
  var data=JSON.stringify({url:window.location.href,html:document.documentElement.innerHTML});var xhr=new XMLHttpRequest();xhr.open('POST','http://localhost:10834/',true);xhr.setRequestHeader('Content-Type','application/json');xhr.send(data);
  ```

  - alternatively, to avoid the console, drag this link (bookmarklet) to your bookmarks toolbar: <a href="javascript:var data=JSON.stringify({url:window.location.href,html:document.documentElement.innerHTML});var xhr=new XMLHttpRequest();xhr.open('POST','http://localhost:10834/',true);xhr.setRequestHeader('Content-Type','application/json');xhr.send(data);">IR-SAVE</a> and click it on any page you want to save (with `browser-listener.py` running)
