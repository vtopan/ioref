"""
ioref: main window.

Author: Vlad Topan (vtopan/gmail)
"""
import functools
import glob
import hashlib
import json
import os
import re
import shlex
import shutil
import subprocess
import sys
import tempfile
import threading
import time
import traceback

from PySide2.QtWidgets import QMainWindow, QApplication, QMessageBox, QShortcut, QWidget, \
    QTextBrowser, QStyleFactory, QLabel, QTextEdit, QPushButton, QLineEdit, QCheckBox, \
    QFileDialog, QListWidget
from PySide2.QtCore import Qt, QObject, Slot
from PySide2.QtGui import QClipboard, QIcon, QKeySequence, QImage, QDesktopServices, \
    QTextDocument

from bs4 import BeautifulSoup


from libizd import IZDFile, VIzdViewer, IZDV_CSS_LIGHT, IZDV_CSS_DARK
from libioqt import create_layouts, VLabelEdit
from iolib import AttrDict
from getdoc import getdoc, OPTIONS as GD_OPTIONS
from ..libioref import *


APP_PATH = os.path.abspath(os.path.dirname(__file__) + '/../..')



class IorMainWindow(QMainWindow):

    def __init__(self, app):
        global MAINWINDOW
        super().__init__()
        MAINWINDOW = self
        self.setWindowTitle('IoRef')
        self.clipboard = QApplication.clipboard()
        self.app = app
        self.setWindowIcon(QIcon(f'{APP_PATH}/ioref/book64.ico'))
        self.create_ui_elements()
        self.setCentralWidget(self.main)
        self.wmap.results.setOpenLinks(False)
        QShortcut(QKeySequence('Esc'), self).activated.connect(self.quit)
        QShortcut(QKeySequence('Alt+F'), self).activated.connect(self.wmap.le_findindocs.edit.setFocus)
        for i in range(1, 10):
            QShortcut(QKeySequence(f'Alt+{i}'), self).activated.connect(functools.partial(self.open_result, i))
        self.cb_dark.edit.stateChanged.connect(self.darkmode_toggled)
        self.wmap.add_doc_clp.clicked.connect(self.add_doc_clp_clicked)
        self.wmap.search.clicked.connect(self.search)
        self.wmap.le_findindocs.edit.returnPressed.connect(self.search)
        self.wmap.le_filter_docs.edit.textChanged.connect(self.update_doclist)
        self.wmap.tabs.currentChanged.connect(self.tab_changed)
        self.wmap.documents.currentRowChanged.connect(self.doclist_select)
        self.wmap.results.anchorClicked.connect(self.result_clicked)
        self.wmap.documents.doubleClicked.connect(self.open_crt_document)
        self.wmap.add_folder.clicked.connect(self.add_folder)
        self.wmap.update_folder.clicked.connect(self.update_folder)
        self.wmap.saveas.clicked.connect(self.export_document)
        self.setFocusPolicy(Qt.StrongFocus)
        self.eng = IorefEngine(self)
        self.ix = self.eng.ix
        self.apply_config()
        self.setStyleSheet(IZDV_CSS_DARK if CFG.ui['dark_mode'] else IZDV_CSS_LIGHT)
        self.wmap.le_findindocs.edit.setFocus()
        self.results = {}


    def apply_config(self):
        """
        Apply = (re?)load configuration.
        """
        CFG.ui = AttrDict(CFG.ui)
        if 'pos' in CFG.ui:
            self.move(*CFG.ui.pos)
        if 'size' in CFG.ui:
            self.resize(*CFG.ui.size)
        if CFG.ui.get('maximized', False):
            self.showMaximized()
        if CFG.ui.get('dark_mode'):
            self.cb_dark.edit.setChecked(True)


    def save_config(self):
        """
        Save configuration.
        """
        CFG['ui']['pos'] = self.pos().toTuple()
        CFG['ui']['size'] = self.size().toTuple()
        CFG['ui']['maximized'] = self.isMaximized()
        json.dump(CFG, open(CFG_FILE, 'w'))


    def create_ui_elements(self):
        self.fieldmap = {}
        self.main = QWidget()
        widgets = {
            '!tab': None,
            'Find Documents': [
                {
                    '_': {
                        'le_findindocs': VLabelEdit('Find documents (Alt+F):'),
                        'search': QPushButton('Search'),
                    },
                    'results': QTextBrowser(),
                    '!vertical': None,
                },
                {
                    'viewer': VIzdViewer(),
                    '_': {
                        'doc_key_terms': VLabelEdit('Key Terms:'),
                        'saveas': QPushButton('Export document'),
                    },
                    '!vertical': None,
                },
                '!horizontal'
            ],
            'Document Manager': [
                [
#                    [
#                        QLabel('Topics:'),
#                        { 'topics': QListWidget(), },
#                    ],
                    [
                        QLabel('Documents:'),
                        { 'documents': QListWidget(), },
                        [
                            QLabel('Details:'),
                            {
                                'doc_did': VLabelEdit('DID:'),
                                'doc_key': VLabelEdit('Key:'),
                            },
                            { 'doc_title': VLabelEdit('Title:'), },
                            { 'doc_path': VLabelEdit('Path:'), },
                            '!border',
                        ],
                        {
                            'le_filter_docs': VLabelEdit('Filter:'),
                            'remove_doc': QPushButton('Remove document'),
                        },
                    ],
                ],
                {
#                    'add_doc_file': QPushButton('Add document from file'),
                    'add_folder': QPushButton('Add folder'),
                    'update_folder': QPushButton('Update folder'),
                    'add_doc_clp': QPushButton('Add document from clipboard (URL or path)'),
                },
            ],
            'Options': {
                '_': QLabel('Options:'),
                'cb_dark': VLabelEdit('Dark mode', etype=QCheckBox),
                '!border': None,
            },
        }
        self.lay, self.wmap = create_layouts({'tabs': widgets})
        self.wmap = AttrDict(self.wmap)
        for k in ('cb_dark', 'viewer', 'results'):
            setattr(self, k, self.wmap[k])
        self.cb_dark.edit.setTristate(False)
        self.main.setLayout(self.lay)


    def ask(self, title, msg, qtype='yesno'):
        """
        Pop a question.
        """
        res = QMessageBox(QMessageBox.Question, title, msg,
                (QMessageBox.Yes|QMessageBox.No) if qtype == 'yesno' else qtype,
                parent=self).exec()
        return res == QMessageBox.Yes if qtype == 'yesno' else res


    def err(self, msg, title='ERROR!'):
        """
        Pop an error.
        """
        self.eng.log(f'{title} {msg}')
        QMessageBox(QMessageBox.Critical, title, msg, QMessageBox.Ok, self).exec()


    def exc(self, title, die=False):
        """
        Pop an exception (and exit).
        """
        self.eng.log(traceback.format_exc())
        self.err(str(sys.exc_info()[1]), title)
        if die:
            self.quit()


    @staticmethod
    def run(args=sys.argv):
        """
        Run app.
        """
        app = QApplication(args)
        app.setStyle(QStyleFactory.create("Plastique"))
        main = IorMainWindow(app)
        main.show()
        sys.exit(app.exec_())


    def quit(self):
        """
        Close app.
        """
        self.save_config()
        self.ix.commit()
        self.app.quit()


    def tab_changed(self, idx):
        """
        Current tab changed.
        """
        if idx == 1:
            # documents
            self.update_doclist()
        elif idx == 2:
            # settings
            ...


    def open_clicked(self):
        """
        Browse for a file to open.
        """
        filename, _ = QFileDialog.getOpenFileName(None, "Open IZD document", "",
                "All Files (*);;IZD Documents (*.izd);;ZIP Files (*.zip)",
                "IZD Documents (*.izd)")
        if filename:
            self.viewer.open(filename)


    def darkmode_toggled(self):
        """
        Apply/remove dark mode CSS.
        """
        CFG.ui['dark_mode'] = self.cb_dark.edit.isChecked()
        self.save_config()
        self.setStyleSheet(IZDV_CSS_DARK if CFG.ui['dark_mode'] else IZDV_CSS_LIGHT)
        self.viewer.set_darkmode(CFG.ui['dark_mode'])


    def add_doc_clp_clicked(self):
        """
        Add document from clipboard URL / path.
        """
        try:
            url = self.clipboard.text().strip('" ') or ''
            if re.search(r'https?://\S+$', url):
                self.eng.download_document(url)
            elif re.search(r'^([a-z]:[/\\]|/)', url, flags=re.I) and os.path.exists(url):
                if os.path.isfile(url):
                    self.eng.add_document(url, store=True)
                elif os.path.isdir(url):
                    for f in glob.glob(url + '/*'):
                        if os.path.isfile(f) and f.rsplit('.', 1)[-1].lower() in KNOWN_EXTS:
                            self.eng.add_document(f, store=True)
            else:
                return self.err('No URL or filename in clipboard!')
        except Exception:
            self.exc(f'Failed adding document from clipboard URL!')
        self.update_doclist()


    def update_doclist(self):
        """
        Update displayed docs.
        """
        flt = self.wmap.le_filter_docs.edit.text().lower()
        self.wmap.documents.clear()
        for key in self.eng.db_docs:
            doc = self.eng.db_docs[key]
            if (not flt) or (flt in doc.title.lower()) or (flt in doc.filename.lower()):
                self.wmap.documents.addItem(f'{doc["did"]}: {doc["title"]}')


    def open_crt_document(self):
        """
        Open the currently selected document.
        """
        if not self.wmap.documents.currentItem():
            return
        did = int(self.wmap.documents.currentItem().text().split(':')[0])
        doc = self.eng.db_get(did)
        self.wmap.tabs.setCurrentIndex(0)
        self.open_document(doc)


    def doclist_select(self):
        """
        An item was selected in the doclist.
        """
        if not self.wmap.documents.currentItem():
            return
        did = int(self.wmap.documents.currentItem().text().split(':')[0])
        doc = self.eng.db_get(did)
        self.wmap.doc_did.edit.setText(str(doc.did))
        self.wmap.doc_key.edit.setText(doc.key)
        self.wmap.doc_title.edit.setText(doc.title)
        self.wmap.doc_path.edit.setText(doc.filename)


    def search(self):
        """
        Search in documents.
        """
        self.search_timer = None
        text = self.wmap.le_findindocs.edit.text()
        results = self.ix.find_document(text)
        resw = self.wmap.results
        resw.clear()
        res = [f'<p>Found {results.estimated_length()} results.</p><ol>']
        cnt = 0
        self.results = {}
        for i, e in enumerate(results):
            hlts = e.highlights('text')
            ftext = re.sub('</?b[^>]*>', '', hlts)[:40].split('\n', 1)[0] if hlts else ''
            did = e['did']
            doc = self.eng.db_get(did)
            doc.ftext = ftext
            self.results[i + 1] = doc
            entry = [f'<a href="doc://res/{i+1}">{doc.title}</a> (DID:{doc.did})']
            if hlts:
                entry.append('<pre>%s</pre>' % re.sub('[\t \r\n]+', ' ', hlts))
            res.append(f'<li>{"".join(entry)}</li>')
            cnt += 1
            if cnt >= 20:
                break
        res.append('</ol>')
        resw.setHtml(''.join(res))


    def result_clicked(self, url):
        """
        Attempt to open result.
        """
        rid = int(url.url().rsplit('/', 1)[-1])
        self.open_result(rid)


    def open_internally(self, doc):
        """
        Open search result in viewer.
        """
        filename, highlight_text = doc.filename, getattr(doc, 'ftext', None)
        ext = filename.rsplit('.', 1)[-1].lower()
        html, render = None, True
        if ext in PANDOC_FORMATS:
            html, _ = run_cmd('pandoc -s %s -t html -o-' % shlex.quote(filename))
        elif ext in CODE_FORMATS:
            stdin = '~~~%s\n%s\n~~~' % (ext, open(filename, 'r').read())
            html, _ = run_cmd('pandoc -s -f markdown -t html -o-', input=stdin.encode('utf8'))
            # todo: --highlight-style zenburn
        elif ext in ('htm', 'html'):
            html = open(filename, 'rb').read().decode()
        elif ext in ('izd',):
            self.viewer.open(filename)
            html = self.viewer.viewer.toHtml()
            render = False
        else:
            html = None
        key_terms = []
        if html:
            if render:
                self.viewer.viewer.setHtml(html)
            # text = re.sub('<[^>]+>', '', html, flags=re.S)
            key_terms = self.ix.extract_key_terms(doc.did)
            if highlight_text:
                self.viewer.viewer.find(highlight_text)
                self.viewer.viewer.find('')
        self.wmap.doc_key_terms.edit.setText(','.join(key_terms))
        return bool(html)


    def open_result(self, rid):
        """
        Open search result (in internal viewer or with associated program).
        """
        if rid not in self.results:
            return
        doc = self.results[rid]
        self.open_document(doc)


    def open_document(self, doc):
        """
        Open a document.
        """
        self.doc = doc
        filename, highlight_text = doc.filename, doc.get('ftext')
        if self.open_internally(doc):
            return
        if os.name == 'nt':
            p = subprocess.Popen(f'start "" "{filename}"', shell=True)
            return
        p = subprocess.Popen(['xdg-open', filename])
        try:
            p.wait(0.5)
            if p.returncode != 0:
                return
        except subprocess.TimeoutExpired:
            pass
        if not highlight_text:
            return
        pid = None
        for i in range(50):
            time.sleep(0.2)
            res, _ = run_cmd("xprop -id $(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}') | awk '/_NET_WM_PID\(CARDINAL\)/{print $NF}'")
            pid = int(res)
            if pid != os.getpid():
                break
        if not pid:
            return
        time.sleep(0.5)
        name = open('/proc/%s/cmdline' % pid, 'rb').read().split(b'\0', 1)[0].decode('utf8')
        name = os.path.basename(name)
        if name in ('okular',):
            # run('xdotool key Alt+E')
            # run('xdotool key F')
            run_cmd('xdotool key control+f')
            run_cmd('xdotool type %s' % shlex.quote(highlight_text))


    def add_folder(self):
        """
        Add a folder recursively, ignore already-indexed documents.
        """
        path = QFileDialog.getExistingDirectory(self, "Select Directory")
        if not path:
            return
        for root, dirs, files in os.walk(path):
            for f in files:
                if f.rsplit('.', 1)[-1].lower() not in KNOWN_EXTS:
                    continue
                self.eng.add_document(os.path.join(root, f), store=True, ignore_if_exists=True)


    def update_folder(self):
        """
        Add a folder recursively, update already-indexed documents.
        """
        path = QFileDialog.getExistingDirectory(self, "Select Directory")
        if not path:
            return
        for root, dirs, files in os.walk(path):
            for f in files:
                if f.rsplit('.', 1)[-1].lower() not in KNOWN_EXTS:
                    continue
                self.eng.add_document(os.path.join(root, f), store=True, update_if_exists=True)


    def export_document(self, doc=None):
        """
        Save the current documetn under another filename.
        """
        doc = doc or self.doc
        if not self.doc:
            return
        ext = self.doc.filename.rsplit('.', 1)[-1].lower()
        def_flt = 'All Files (*)'
        filters = [def_flt]
        if len(ext) <= 4:
            def_flt = f'{ext.upper()} Files (*.{ext})'
            filters.append(def_flt)
        filename, _ = QFileDialog.getOpenFileName(None, "Save as...", doc.title, ';;'.join(filters), def_flt)
        if filename:
            try:
                shutil.copy(doc.filename, filename)
            except Exception:
                self.exc(f'Failed saving document copy!')
