"""
Main libary for the ioref project.

Author: Vlad Topan (vtopan/gmail)
"""
import functools
import hashlib
import glob
import json
import os
import re
import shlex
import shutil
import subprocess
import tempfile
import threading
import time

from whoosh import index as wsh_index
from whoosh import fields as wsh_fields
from whoosh.qparser import QueryParser

from bs4 import BeautifulSoup

import PyPDF2

from libizd import IZDFile
from iolib import AttrDict
from iolib.db import DataCache
from getdoc import getdoc, OPTIONS as GD_OPTIONS


APP_PATH = os.path.abspath(os.path.dirname(__file__) + '/..')
CFG_FILE = f'{APP_PATH}/ioref-config.json'
CFG = AttrDict(ui=AttrDict(dark_mode=False),
        http_cache_dir=f'{APP_PATH}/data/dld-cache',
        index_dir=f'{APP_PATH}/data/doc-index',
        doc_storage=f'{APP_PATH}/data/doc-storage',
        db_docs=f'{APP_PATH}/data/documents.db',
        db_keys=f'{APP_PATH}/data/keys.db',
        db_filenames=f'{APP_PATH}/data/filenames.db',
        logfile=f'{APP_PATH}/data/ioref.log')
PDF_CONVERTER = None
PANDOC_FORMATS = ('md', 'rst')
CODE_FORMATS = ('py', 'pl', 'c', 'cpp', 'java', 'h', 'hpp', 'rb', 'php')
KNOWN_EXTS = PANDOC_FORMATS + CODE_FORMATS + ('izd', 'pdf', 'htm', 'txt')
EXT_MAP = {
    'markdown': 'md',
    'html': 'htm',
}
LOCKS = {e:threading.Lock() for e in ('did', 'log')}


__VER__ = '0.1.0.20220126'


def parse_doc(filename, title=None):
    """
    Parse a document, extract title, text and calculate key.
    """
    res = AttrDict()
    ext = filename.rsplit('.', 1)[-1].lower()
    ext = res.ext = EXT_MAP.get(ext, ext)[:4]
    if ext not in KNOWN_EXTS:
        raise ValueError('Invalid file format: {ext}!')
    xtitle = None
    if ext in ('izd', 'htm'):
        if ext == 'izd':
            iz = IZDFile(filename)
            html = iz.text
        else:
            html = open(filename).read()
        bs = BeautifulSoup(html, 'lxml')
        xtitle = bs.title.text.strip()
        res.text = bs.text
    elif ext in ('md', 'txt', 'rst') + CODE_FORMATS:
        # todo: full pandoc header/title support
        res.text = open(filename).read()
        if ext == 'md':
            m = re.search('^##? (.+)', res.text)
            if not m:
                raise ValueError('Failed extracting title from Markdown document {filename}!')
            xtitle = m.groups()[0].strip()
    elif ext == 'pdf':
        xtitle, res.text = parse_pdf(filename)
    res.title = title or xtitle or os.path.basename(filename)
    res.key = hashlib.sha256(res.text.encode()).hexdigest()
    res.filename = filename
    return res


def parse_pdf(filename):
    """
    Parse a PDF document, return title and text.
    """
    global PDF_CONVERTER
    if PDF_CONVERTER is None:
        output, _ = run_cmd('pdftotext -v', include_err=True)
        if output:
            # use if available - faster & better results
            PDF_CONVERTER = 'pdftotext'
        else:
            PDF_CONVERTER = 'pypdf2'
    fh = open(filename, 'rb')
    pdf = PyPDF2.PdfFileReader(fh)
    title = None
    if pdf.documentInfo:
        title = str(pdf.documentInfo.title)
    if PDF_CONVERTER == 'pypdf2':
        lst = []
        for i in range(0, pdf.getNumPages()):
            lst.append(pdf.getPage(i).extractText())
        text = '\n\n'.join(lst).strip()
    elif PDF_CONVERTER == 'pdftotext':
        of = tempfile.mktemp(suffix='.txt')
        run_cmd(f'pdftotext "{filename}" "{of}"')
        text = open(of).read().strip()
        os.remove(of)
    else:
        raise ValueError(f'Unknown PDF text extractor: {PDF_CONVERTER}!')
    return title, text


def run_cmd(cmd, include_err=False, raw=False, **kargs):
    if 'stdout' not in kargs:
        if include_err:
            kargs['stdout'] = subprocess.PIPE
            kargs['stderr'] = subprocess.STDOUT
        else:
            kargs['capture_output'] = True
    p = subprocess.run(cmd, shell=True, check=False, **kargs)
    output = p.stdout
    if not raw:
        output = output.decode('utf8')
    return output, p.returncode


def load_config():
    """
    Load configuration file.
    """
    if os.path.isfile(CFG_FILE):
        CFG.update(json.load(open(CFG_FILE)))
    GD_OPTIONS['cache_dir'] = CFG['http_cache_dir']
    GD_OPTIONS['dbg_keepattrs'] = False
    return CFG



class Indexer:

    def __init__(self, path):
        self.path = None
        self.set_path(path)


    def set_path(self, path):
        """
        Set index path (and initialize if necessary).
        """
        if path == self.path:
            return
        self.path = path
        if os.path.isdir(path) and glob.glob(f'{path}/*.toc'):
            self.wsh = wsh_index.open_dir(path)
        else:
            os.makedirs(path)
            self.wsh = wsh_index.create_in(path, wsh_fields.Schema(title=wsh_fields.TEXT(stored=True),
                    text=wsh_fields.TEXT(stored=True),
                    tags=wsh_fields.KEYWORD(scorable=True, commas=True),
                    did=wsh_fields.NUMERIC(stored=True, unique=True),
                    key=wsh_fields.ID(stored=True, unique=True)))
        self._writer = None
        self.written = 0
        self.qparser = QueryParser("text", self.wsh.schema)


    @property
    def writer(self):
        """
        Returns an index writer.
        """
        if self._writer is None:
            self._writer = self.wsh.writer()
        return self._writer


    def commit(self, verbose=True):
        """
        Commit added documents to the index.
        """
        if self.written:
            self.writer.commit()
        self._writer = None
        self.written = 0


    def add_document(self, doc, commit=False):
        """
        Index a document (as returned by parse_doc).
        """
        tags = []   # [todo]
        self.writer.add_document(title=doc.title, text=doc.text, tags=tags, did=doc.did, key=doc.key)
        self.written += 1
        if commit:
            self.commit()


    def find_document(self, query):
        """
        Search the index.
        """
        query = self.qparser.parse(query)
        results = self.wsh.searcher().search(query)
        return results


    def extract_key_terms(self, did):
        """
        Extract the key terms from a document (given by path).
        """
        s = self.wsh.searcher()
        doc_num = s.document_number(did=did)
        return [keyword for keyword, score in s.key_terms([doc_num], "text")]
        # return [keyword for keyword, score in self.wsh.searcher().key_terms_from_text('text', text)]




class IorefEngine:

    def __init__(self, ui):
        load_config()
        self.ui = ui
        self.db_docs = DataCache(CFG['db_docs'], max_age=0)
        self.db_keys = DataCache(CFG['db_keys'], max_age=0)
        self.db_filenames = DataCache(CFG['db_filenames'], max_age=0)
        CFG.doc_storage = os.path.abspath(CFG.doc_storage)
        if not os.path.isdir(CFG.doc_storage):
            os.makedirs(CFG.doc_storage)
        self.ix = Indexer(CFG.index_dir)


    def add_document(self, filename, store=False, update_if_exists=False, ignore_if_exists=False):
        """
        Add document from file.
        """
        filename = os.path.abspath(filename)
        fun = self.ix.add_document
        if filename in self.db_filenames:
            if update_if_exists:
                self.log(f'Updating {filename}...')
                fun = functools.partial(self.ix.update_document)
            else:
                return self.ui.err(f'Document {filename} is already indexed!')
        try:
            doc = parse_doc(filename)
            if doc.key in self.db_keys:
                title, oldfn = self.db_keys[doc.key]
                if ignore_if_exists:
                    self.log(f'Skipping already-indexed {filename} (was {title} @ {oldfn})...')
                    return
                if update_if_exists:
                    if oldfn.startswith(CFG.doc_storage):
                        os.remove(oldfn)
                    self.log(f'Updating {title} / {doc.key}...')
                    fun = functools.partial(self.ix.update_document)
                else:
                    return self.ui.err(f'Document {filename} already exists in DB as {title} @ {oldfn}!')
            # attempt to store?
            if store:
                nfn = os.path.abspath(self.doc_storage_path(doc))
                if os.path.isfile(nfn):
                    os.remove(nfn)
                path = os.path.dirname(nfn)
                if not os.path.isdir(path):
                    os.makedirs(path)
                shutil.copy(filename, nfn)
                filename = nfn
            doc.filename = filename
            self.db_add_doc(doc)
            # attempt to index
            fun(doc, commit=True)
            self.log(f'Added {doc.title} @ {filename} ({doc.key}).')
        except Exception:
            self.ui.exc(f'Failed adding document {filename}!')


    def download_document(self, url, content=None):
        """
        Download document (blog post, etc.) and index it.
        """
        of = tempfile.mktemp(suffix='.izd')
        with tempfile.TemporaryDirectory() as tempd:
            getdoc(url, of, tempd, content=content)
        if not os.path.isfile(of):
            return self.ui.err('Failed converting document to IZD!')
        self.add_document(of, store=True)
        os.remove(of)


    def doc_storage_path(self, doc):
        """
        Generates the storage path for the doc.
        """
        return f'{CFG.doc_storage}/{doc.key[:2]}/{doc.key[2:4]}/{doc.key}.{doc.ext}'


    def db_get(self, did):
        """
        Retrieves an entry from the DB.
        """
        return AttrDict(self.db_docs[did])


    def db_add_doc(self, doc):
        """
        Add a (parsed) document to the DB.
        """
        LOCKS['did'].acquire()
        doc.did = len(self.db_docs) + 1
        obj = {'key': doc.key, 'title': doc.title, 'filename': doc.filename, 'did': doc.did}
        self.db_docs[doc.did] = obj
        LOCKS['did'].release()
        self.db_keys[doc.key] = (doc.title, doc.filename)
        self.db_filenames[doc.filename] = True
        return doc.did


    def log(self, msg):
        """
        Print+log a (debug) message.
        """
        print(msg)
        if CFG.logfile:
            open(CFG.logfile, 'a').write(f'[{time.strftime("%H:%M:%S, %d.%m.%y")}] {msg}\n')

