#!/usr/bin/env python3
"""
Listens on http://localhost:10834/ for the browser request to add a document (with
the full HTML contents of the current page + URL).

Run:

var data=JSON.stringify({url:window.location.href,html:document.documentElement.innerHTML});var xhr=new XMLHttpRequest();xhr.open('POST','http://localhost:10834/',true);xhr.setRequestHeader('Content-Type','application/json');xhr.send(data);

in the browser console to trigger.

Todo:
- take image contents as well
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import os
import urllib.parse
import sys
import traceback

from bs4 import BeautifulSoup

IOR_PATH = os.path.dirname(__file__)
sys.path += [f'{IOR_PATH}/io-scripts', f'{IOR_PATH}/iolib', f'{IOR_PATH}/libioqt', f'{IOR_PATH}/izd-tools']
from ioref import load_config, Indexer, CFG, IorefEngine
from getdoc import getdoc



class IoRefServer(BaseHTTPRequestHandler):

    def _send_headers(self):
        self.send_response(200)
        self.send_header('Content-Type', 'text/plain')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Headers', 'content-type')
        self.end_headers()


    def do_OPTIONS(self):
        self._send_headers()


    def do_POST(self):
        data = self.rfile.read(int(self.headers['Content-Length'])).decode()
        # data = urllib.parse.unquote_plus(data)
        data = json.loads(data)
        self._send_headers()
        ENG.download_document(data['url'], content=data['html'].encode())



class ConUI:

    def err(self, msg, title='ERROR!'):
        sys.stderr.write(f'{title} {msg}\n')


    def exc(self, title, die=False):
        self.err(str(sys.exc_info()[1]), title)
        sys.stderr.write(traceback.format_exc())
        if die:
            sys.exit()



def run():
    global ENG
    load_config()
    port = int(sys.argv[1]) if len(sys.argv) > 1 else 10834
    print(f'Listening on port {port}, press Ctrl+C to stop...')
    httpd = HTTPServer(('127.0.0.1', port), IoRefServer)
    ENG = IorefEngine(ConUI())
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        sys.exit('Ctrl+C pressed, stopping...')
    httpd.server_close()


run()
